\documentclass[a4paper,11pt,fleqn]{article}
\usepackage{mathpazo,amsmath,amsfonts,amssymb,siunitx}
\usepackage[margin=20mm]{geometry}
\newcommand*{\md}[1]{\mathrm{d}#1}
\newcommand*{\mT}{\mathrm{T}}
\newcommand*{\eqsref}[1]{Eq.~(\ref{#1})}
\newcommand*{\norm}[1]{\left\lVert#1\right\rVert}
\newcommand*{\pdfrac}[2]{\dfrac{\partial#1}{\partial#2}}
\newcommand*{\ddfrac}[2]{\dfrac{\md#1}{\md#2}}
\setlength\parindent{0pt}
\setlength\parskip{1em}
\begin{document}
\section{Flow Rule}
The plastic potential is defined as
\begin{gather}
G=g_1^2+q^2_{h1}g_2,
\end{gather}
where
\begin{gather}
g_1=\left(1-q_{h1}\right)g_3^2+\sqrt{\dfrac{3}{2}}\dfrac{s}{f_c},\\
g_2=\dfrac{m_0s}{\sqrt{6}f_c}+\dfrac{m_g}{f_c},\\
g_3=\dfrac{s}{\sqrt{6}f_c}+\dfrac{p}{f_c},\\
m_g=A_gB_gf_c\exp\left(\dfrac{3p-q_{h2}f_t}{3B_gf_c}\right),\\
A_g=\dfrac{3f_tq_{h2}}{f_c}+\dfrac{m_0}{2},\\
B_g=\dfrac{q_{h2}\left(1+f_t/f_c\right)/3}{\ln{}A_g-\ln\left(2D_f-1\right)-\ln\left(3q_{h2}+m_0/2\right)+\ln\left(D_f+1\right)}.
\end{gather}
With the derivatives of auxiliary functions expressed as
\begin{gather}
\pdfrac{g_3}{p}=\dfrac{1}{f_c},\qquad\pdfrac{g_3}{s}=\dfrac{1}{\sqrt{6}f_c},\qquad
\pdfrac{g_2}{p}=\dfrac{1}{f_c}\pdfrac{m_g}{p},\qquad\pdfrac{g_2}{s}=\dfrac{m_0}{\sqrt{6}f_c},\\
\pdfrac{g_1}{p}=2\left(1-q_{h1}\right)g_3\pdfrac{g_3}{p},\qquad\pdfrac{g_1}{s}=2\left(1-q_{h1}\right)g_3\pdfrac{g_3}{s}+\sqrt{\dfrac{3}{2}}\dfrac{1}{f_c}.
\end{gather}
The flow rule can be derived as
\begin{gather}
G_p=\pdfrac{G}{p}=2g_1\pdfrac{g_1}{p}+q^2_{h1}\pdfrac{g_2}{p}=\dfrac{4\left(1-q_{h1}\right)g_1g_3+q^2_{h1}A_g\exp\left(\dfrac{p-q_{h2}f_t/3}{B_gf_c}\right)}{f_c},\\
G_s=\pdfrac{G}{s}=2g_1\pdfrac{g_1}{s}+q^2_{h1}\pdfrac{g_2}{s}=\dfrac{4\left(1-q_{h1}\right)g_1g_3+6g_1+m_0q^2_{h1}}{\sqrt{6}f_c}.
\end{gather}
\section{Yield Function}
\begin{gather}
F=g_1^2+m_0q^2_{h1}q_{h2}g_3-q^2_{h1}q^2_{h2},
\end{gather}
where
\begin{gather}
m_0=\dfrac{3f_c^2-3f_t^2}{f_cf_t}\dfrac{e}{1+e}.
\end{gather}
The partial derivatives are
\begin{gather}
\pdfrac{F}{p}=2g_1\pdfrac{g_1}{p}+m_0q^2_{h1}q_{h2}\pdfrac{g_3}{p},\\
\pdfrac{F}{s}=2g_1\pdfrac{g_1}{s}+m_0q^2_{h1}q_{h2}\pdfrac{g_3}{s},\\
\pdfrac{F}{\kappa_p}=2g_1\pdfrac{g_1}{\kappa_p}+2m_0q_{h1}q_{h2}g_3\ddfrac{q_{h1}}{\kappa_p}+m_0q^2_{h1}g_3\ddfrac{q_{h2}}{\kappa_p}-2q_{h1}q_{h2}\left(q_{h2}\ddfrac{q_{h1}}{\kappa_p}+q_{h1}\ddfrac{q_{h2}}{\kappa_p}\right).
\end{gather}
\section{Plasticity Residual}
\begin{gather}
\mathbold{R}=\left\{
\begin{array}{l}
g_1^2+m_0q^2_{h1}q_{h2}g_3-q^2_{h1}q^2_{h2},\\[4mm]
s+2G\gamma{}G_s-s^{tr},\\[4mm]
p+K\gamma{}G_p-p^{tr},\\[4mm]
x_h\kappa_p^n+\gamma{}G_\kappa-x_h\kappa_p.
\end{array}
\right.
\end{gather}
In which, $G_\kappa=\sqrt{G_s^2+G_p^2/3}$.

The Jacobian reads
\begin{gather}
\mathbold{J}=\begin{bmatrix}
\cdot&\pdfrac{F}{s}&\pdfrac{F}{p}&\pdfrac{F}{\kappa_p}\\[6mm]
2GG_s&1+2G\gamma\pdfrac{G_s}{s}&2G\gamma\pdfrac{G_s}{p}&2G\gamma\pdfrac{G_s}{\kappa_p}\\[6mm]
KG_p&K\gamma\pdfrac{G_p}{s}&1+K\gamma\pdfrac{G_p}{p}&K\gamma\pdfrac{G_p}{\kappa_p}\\[6mm]
G_\kappa&\gamma\pdfrac{G_\kappa}{s}&\left(\kappa_p^n-\kappa_p\right)\ddfrac{x_h}{p}+\gamma\pdfrac{G_\kappa}{p}&\gamma\pdfrac{G_\kappa}{\kappa_p}-x_h.
\end{bmatrix}
\end{gather}
\section{Damage}
\subsection{Tension}
The equivalent strain is expressed as a function of damage factors.
\begin{gather}
\varepsilon_i=\kappa_{dt1}+\omega_t\kappa_{dt2}.
\end{gather}
While the uniaxial stress response can be expressed as
\begin{gather}
\sigma=\left(1-\omega_t\right)E\kappa_{dt}.
\end{gather}
Assume an exponential degradation curve,
\begin{gather}
\sigma=f_t\exp\left(-\dfrac{\varepsilon_i}{\varepsilon_{fc}}\right)
\end{gather}

The local residual is
\begin{gather}
R=f_t\exp\left(-\dfrac{\varepsilon_i}{\varepsilon_{fc}}\right)-\left(1-\omega_t\right)E\kappa_{dt}.
\end{gather}
\begin{gather}
\pdfrac{R}{\omega_t}=E\kappa_{dt}-\dfrac{\kappa_{dt2}}{\varepsilon_{fc}}f_t\exp\left(-\dfrac{\varepsilon_i}{\varepsilon_{fc}}\right),\\
\pdfrac{R}{\kappa_{dt}}=-\left(1-\omega_t\right)E,\\
\pdfrac{R}{\kappa_{dt1}}=-\dfrac{1}{\varepsilon_{fc}}f_t\exp\left(-\dfrac{\varepsilon_i}{\varepsilon_{fc}}\right),\\
\pdfrac{R}{\kappa_{dt2}}=-\dfrac{\omega_t}{\varepsilon_{fc}}f_t\exp\left(-\dfrac{\varepsilon_i}{\varepsilon_{fc}}\right).
\end{gather}
\subsection{Compression}
Similarly,
\begin{gather}
\varepsilon_i=\kappa_{dc1}+\omega_c\kappa_{dc2}.
\end{gather}
\end{document}
